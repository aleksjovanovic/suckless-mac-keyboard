# suckless-mac-keyboard

Keyboard layouts for Mac, created with ukelele that doesn't suck.

## Explanation

### US_Int_HHKB
Proper US International keyboard layout without deadkeys, check (https://en.wikipedia.org/wiki/British_and_American_keyboards) for further details. Please keep in mind that not all characters are propperly mapped, only umlauts.

### US_Int_No_Touchbar
TODO: Goal is to find a way around the messy touchbar

### DE_No_Deadkeys
TODO: German keyboard layout without deadkeys
